#!/usr/bin/env python3

# Start with imports.
import pw_gui as pw


# Define object classes and functions here.


def main():
    # this is where the main body of code goes.
    myApp = pw.PwGui("Password Generator")
    myApp.mainloop()


if __name__ == '__main__':
    main()

# end of program
