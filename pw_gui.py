# This is a sample Python script.

# Start with imports.
import tkinter as tk
import pwgen as pw


# Define object classes and functions here.
class PwGui(tk.Tk):
    def __init__(self, appName):
        super().__init__()
        # self.geometry("400x600")
        self.title(str(appName))
        # self.config(background="light green")
        print("Starting UI")
        self.newPassword = pw.PwGen()
        self._create_app_frame()
        self.generated_password = ''

    def _create_app_frame(self):

        def submit_size():
            pwSize = int(sizeInput.get())
            # try except might be good option here
            self.newPassword.pw_size = pwSize
            sizeInput.delete(0, 4)
            sizeInput.insert(0, str(self.newPassword.pw_size))

        def setCharRepeat():
            if rpt.get():
                self.newPassword.no_repeat = True
                print("turn character repeat suppression on")

            else:
                self.newPassword.no_repeat = False
                print("turn character repeat suppression off")
            return self.newPassword.no_repeat

        def setLowerCase():
            if exc_lower.get():
                self.newPassword.exc_lower = True
                print("turn exclude lower on")

            else:
                self.newPassword.exc_lower = False
                print("turn exclude lower off")
            return self.newPassword.exc_lower

        def setUpperCase():
            if exc_upper.get():
                self.newPassword.exc_upper = True
                print("turn exclude upper on")

            else:
                self.newPassword.exc_upper = False
                print("turn exclude upper off")
            return self.newPassword.exc_upper

        def setNumber():
            if exc_number.get():
                self.newPassword.exc_number = True
                print("turn exclude number on")

            else:
                self.newPassword.exc_number = False
                print("turn exclude number off")
            return self.newPassword.exc_number

        def setSpecial():
            if exc_special.get():
                self.newPassword.exc_special = True
                print("turn exclude special on")

            else:
                self.newPassword.exc_special = False
                print("turn exclude special off")
            return self.newPassword.exc_special

        def generatePassword():
            self.generated_password = self.newPassword.generatePassword()
            outputLabel.config(text=f"Your new password is: \n{self.generated_password}")

        def copyPassword():
            appFrame.clipboard_clear()
            appFrame.clipboard_append(self.generated_password)

        appFrame = tk.Frame(master=self, pady=5)
        appFrame.pack()
        tk.Label(master=appFrame, text="My Password Generator", font="Ariel 14 bold", padx=10).pack()
        tk.Label(master=appFrame, text="Password Length", font="Ariel 14").pack()
        sizeDesc = tk.Label(
            master=appFrame,
            text="default value is 12 characters. \nTo change the length, \nenter a new value and submit",
            padx=10,
            pady=5,
        )
        sizeDesc.pack()
        sizeEntryFrame = tk.Frame(master=appFrame, bd=1, relief="raised")
        sizeEntryFrame.pack()
        sizeButton = tk.Button(master=sizeEntryFrame, text="Submit", command=submit_size)
        sizeButton.pack(side="right")
        sizeInput = tk.Entry(master=sizeEntryFrame, width=5)
        sizeInput.insert(0, str(self.newPassword.pw_size))
        sizeInput.pack(side="right")
        sizeLabel = tk.Label(master=sizeEntryFrame, text="Password Length", padx=5)
        sizeLabel.pack(side="right")
        tk.Label(master=appFrame, text="Password Options", font="Ariel 12", pady=4).pack()
        # password option checkboxes
        rpt = tk.BooleanVar()
        rpt.set(self.newPassword.no_repeat)
        rpt_chr_button = tk.Checkbutton(master=appFrame,
                                        text="No Duplicate Characters",
                                        variable=rpt,
                                        onvalue=True,
                                        offvalue=False,
                                        command=setCharRepeat,
                                        pady=4,
                                        )
        rpt_chr_button.pack()
        exc_lower = tk.BooleanVar()
        exc_lower.set(self.newPassword.exc_lower)
        exc_lower_button = tk.Checkbutton(master=appFrame,
                                          text="Exclude Lower Case",
                                          variable=exc_lower,
                                          onvalue=True,
                                          offvalue=False,
                                          command=setLowerCase,
                                          pady=4,
                                          )
        exc_lower_button.pack()
        exc_upper = tk.BooleanVar()
        exc_upper.set(self.newPassword.exc_upper)
        exc_upper_button = tk.Checkbutton(master=appFrame,
                                          text="Exclude Upper Case",
                                          variable=exc_upper,
                                          onvalue=True,
                                          offvalue=False,
                                          command=setUpperCase,
                                          pady=4,
                                          )
        exc_upper_button.pack()
        exc_number = tk.BooleanVar()
        exc_number.set(self.newPassword.exc_number)
        exc_number_button = tk.Checkbutton(master=appFrame,
                                           text="Exclude Numbers",
                                           variable=exc_number,
                                           onvalue=True,
                                           offvalue=False,
                                           command=setNumber,
                                           pady=4,
                                           )
        exc_number_button.pack()
        exc_special = tk.BooleanVar()
        exc_special.set(self.newPassword.exc_special)
        exc_special_button = tk.Checkbutton(master=appFrame,
                                            text="Exclude Special Characters",
                                            variable=exc_special,
                                            onvalue=True,
                                            offvalue=False,
                                            command=setSpecial,
                                            pady=4,
                                            )
        exc_special_button.pack()

        getPasswordButton = tk.Button(master=appFrame,
                                      text="Generate Password",
                                      command=generatePassword,
                                      bg="green",
                                      fg="white",
                                      )
        getPasswordButton.pack()
        outputLabel = tk.Label(master=appFrame,
                               text=f"Password will \nappear here",
                               font="Ariel 12",
                               wraplength=300,
                               pady=10
                               )
        outputLabel.pack()
        copy_button = tk.Button(master=appFrame,
                                text="Copy Password",
                                command=copyPassword,
                                bg="blue",
                                fg="white",
                                )
        copy_button.pack()


def main():
    # this is where the main body of code goes.
    # create pwgen object either here or in UI object
    # create UI object
    myApp = PwGui("Password Generator")
    # myApp.createAppFrame()
    myApp.mainloop()


if __name__ == '__main__':
    main()

# end of program
