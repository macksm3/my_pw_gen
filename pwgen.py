# password generator module

import random


class PwGen:
    """Generate a password using random characters pulled out of a master list of characters"""
    chr_lower = "abcdefghijklmnopqrstuvwxyz"
    chr_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    chr_number = "1234567890"
    chr_special = "@%+\\/'!#$^?:,(){}[]~`-_."
    exc_lower = False
    exc_upper = False
    exc_number = False
    exc_special = False
    no_repeat = True

    def __init__(self):
        self.pw_string = ""
        self.pw_size = 12
        self.characterList = ""
        self.characterSet = set()
        print("Starting the password generator")

    def setLength(self, pw_size):
        self.pw_size = pw_size
        return f'Your new password will be {self.pw_size} characters long'
        # print(f'Your password will be {self.pw_size} characters long')

    # methods to set excluded character sets to True and back
    # using checkboxes for each one

    def generatePassword(self):
        self.pw_string = ""
        self.characterList = ""
        if not self.exc_lower:
            self.characterList = self.characterList + self.chr_lower
        if not self.exc_upper:
            self.characterList = self.characterList + self.chr_upper
        if not self.exc_number:
            self.characterList = self.characterList + self.chr_number
        if not self.exc_special:
            self.characterList = self.characterList + self.chr_special
        # get included characters and run random selection loop
        # characterSet needs to be a set so characters are included only once
        if self.exc_lower and self.exc_upper and self.exc_number and self.exc_special:
            self.pw_string = "no characters to choose from"
        else:
            if self.no_repeat:
                if len(self.characterList) < self.pw_size:
                    # pop up message box here
                    print("not enough characters")
                else:
                    while len(self.characterSet) < self.pw_size:
                        chr_random = random.randint(0, len(self.characterList) - 1)
                        self.characterSet.add(self.characterList[chr_random])
                    # print(self.characterSet)
                    while len(self.characterSet) > 0:
                        self.pw_string = self.pw_string + self.characterSet.pop()
            else:
                while len(self.pw_string) < self.pw_size:
                    chr_random = random.randint(0, len(self.characterList) - 1)
                    self.pw_string = self.pw_string + self.characterList[chr_random]
            # print(self.pw_string)
        return self.pw_string


def main():
    # this is where the main body of code goes.
    newPassword = PwGen()
    print(f"Current password length is {newPassword.pw_size}")
    print(newPassword.setLength(16))
    print(newPassword.generatePassword())


if __name__ == '__main__':
    main()
